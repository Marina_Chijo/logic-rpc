# Bare RPC

Repositório base para criação de testes automatizados utilizando Gherkin, Behave e Appium ou Jeffium/AltUnityDriver como drivers.

## Setup
Requisitos
* [Python 3.5.8](https://www.python.org/downloads/)  ou superior 
* [PIP](https://pip.pypa.io/en/stable/installing/)
* [Gherkin](https://pypi.org/project/gherkin/)
```
pip install gherkin
```
* [Alt Unity Runner 1.4.0](https://pypi.org/project/altunityrunner/1.4.0/)
```
pip install altunityrunner==1.4.0
```
Unity Package:
* [Alt Unity Tester 1.0.17](https://prod.intra.tappsgames.com/c01a431f-b46c-44b8-b10e-979db2416b63/-/web/detail/com.tappsgames.alt-unity-tester)
   * Para acessar pelo Unity, vá em Windows>Packager Manager

Acesse o arquivo behave.ini dentro da pasta raiz do projeto e ajuste o project_path para a pasta do projeto Unity
```ini
#the path to read json created by Unity Node Editor
project_path=C:/Git/barebundleproject/
```
Para Androidd,  ajustar as variáveis abaixo de acordo com a informação necessária
```ini
# name of the apk package
appPackage=br.com.tapps.barebundleid

# path for the apk in case the app is not installed on device
app_path=C:/Git/AppiumTest/barebundleid.apk
```

### Como executar

Rodar behave no simulador, usando o AltUnity como driver:
```sh
$ behave
```

Rodar behave no Android, usando o Appium como driver:
```sh
$ behave -D platform=android
```

### Documentações

A seguir segue algumas páginas do confluence com maiores informações:

| Título | Link |
| ------ | ------ |
| Arquitetura básica da ferramenta | https://tappsgames.atlassian.net/wiki/spaces/TL/pages/895287528/Arquitetura+b+sica+da+ferramenta |
| HelixBridge e testes automatizados | https://tappsgames.atlassian.net/wiki/spaces/GP/pages/821100674/HelixBridge+e+testes+automatizados |
| Treinamento - Automação com Helix Bridge | https://tappsgames.atlassian.net/wiki/spaces/QA/pages/715098107/Treinamento+-+Automa+o+com+Helix+Bridge |
| Treinamento BDD e Gherkin | https://tappsgames.atlassian.net/wiki/spaces/QA/pages/567345653/Treinamento+BDD+e+Gherkin |

### TO DO

 - Melhorar esse ReadMe