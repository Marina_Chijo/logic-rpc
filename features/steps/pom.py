from time import sleep

#Global variables
currentPage = {}
focusedElement = {}
focusedElements = {}



@then(u'the "{pageObject}" should be loaded')
def step_impl(context,pageObject):
    global currentPage
    currentPage = context.POM_MAP[pageObject]
    currentPage.load_confirm()


@given(u'the "{pageObject}" page is loaded')
def load_page(context,pageObject):
    global currentPage
    currentPage = context.POM_MAP[pageObject]
    print(str.format('Page loaded is: {}',currentPage.name))
