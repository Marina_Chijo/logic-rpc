from time import sleep
from helixbridge import HelixElement


@given(u'PrivacyPolicy ended')
def step_impl(context):
    context.helixbridge.send_command(
        "gamePackage('PrivacyPolicyConsentPopup'):closePopup()")
