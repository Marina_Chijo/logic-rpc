import subprocess
import requests
def process_is_running(process_name: str):
    s = subprocess.check_output('tasklist', shell=True)
    lines = str(s).split('\\n')
    for x in lines:
        if process_name in x:
            print(x)
            return True
    return False

def check_appium_session():
    try:
        response = requests.get('http://127.0.0.1:4723/wd/hub/sessions')
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except Exception as err:
        print('Appium session not found') 
        return False
    else:
        print('Appium is already started')
