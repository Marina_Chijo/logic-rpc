import os
import utils
import json
from altunityrunner import AltrunUnityDriver
from appium import webdriver
from behave import fixture, use_fixture
from time import sleep
from pageobjectmodel import *

# PATH = lambda p: os.path.abspath(os.path.join(os.path.dirname(__file__), p))


def read_page_object_from_json(context):
    #Read JSON data into the pageObjects variable


    
    jsonPath =  context.config.userdata['project_path'] + context.config.userdata['json_path']
    if not os.path.exists(jsonPath):
        print('Json Nodes was not found on defined path')
        return
    if jsonPath:
        with open(jsonPath, 'r') as f:
            json_array = json.load(f)
    POM_MAP = {}
    for json_page_object in json_array:
        POM_MAP.update({json_page_object['name'] : PageObject(context.altdriver, json_page_object)})
    context.POM_MAP = POM_MAP

def treat_transcendent_page_objects(context):
    for page_name,page_object in context.POM_MAP.items():
        if page_object.is_transcendental:
            for connected_page_name in page_object.page_connections:
                context.POM_MAP[connected_page_name].translator.update(page_object.translator)
                context.POM_MAP[connected_page_name].group_translator.update(page_object.group_translator)
                context.POM_MAP[connected_page_name].element_connections.update(page_object.element_connections)
                pass
            pass
        pass

def setup_android(desired_caps):
    desired_caps['platformName'] = 'Android'
    desired_caps['deviceName'] = 'device'
    # remove appPackage if you like to intall the new apk
    # desired_caps['appPackage'] = 'br.com.tapps.decordream'
    # keep in mind that adb validade versions, so if you have
    # an recent apk, it will check and install if the device
    # have an older version
    desired_caps['appPackage'] = 'br.com.tapps.decordream'
    desired_caps['app'] = 'C:\\Git\\AppiumTest\\homeside2-withprefab.apk'
    # desired_caps['app'] = 'C:\\Git\\AppiumTest\\homesideAUT22.apk'
    desired_caps['adbExecTimeout'] = '15000'
    desired_caps['newCommandTimeout'] = '15000'


def setup_ios(desired_caps):
    desired_caps['platformName'] = 'iOS'
    desired_caps['deviceName'] = 'iPhone5'
    desired_caps['automationName'] = 'XCUITest'
    desired_caps['app'] = PATH('../../Builds/decordream.ipa')


@fixture
def appium_android(context):
    userdata = context.config.userdata
    platform = userdata['platform']

    desired_caps = {}
    if platform == "desktop":
        platform = None
        driver = None
    else:
        if platform == "android":
            setup_android(desired_caps)
        else:
            setup_ios(desired_caps)
        driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)

    altdriver = AltrunUnityDriver(driver, platform)
    context.altdriver = altdriver
    read_page_object_from_json(context)
    treat_transcendent_page_objects(context)
    # context.events = Events(altdriver)

    yield altdriver

    print("Stopping AltDriver")
    altdriver.stop()
    if driver:
        driver.quit()
    
def before_all(context):
    use_fixture(appium_android, context)
    # -- HINT: CLEANUP-FIXTURE is performed after after_all() hook is called.

