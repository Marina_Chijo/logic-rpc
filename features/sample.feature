@sample_tag
Feature: Sample Feature
	
	@another_sample_tag
    Scenario: Sample Scenario
    	Given the "Home Screen" page is loaded
    	When I click on "Play Button"
    	Then the "Main Menu" should appears
    	
    @yet_another_tag
    Scenario Outline: Testing buyable itens by HC
        Given that I noted how many "HC" I have
    	When I click on "<item>"
    	Then "<buy_button>" should appears
    	When I click on "<buy_button>"
    	Then the "HC" should decrease by "<price_tag>" 
    	And "<buy_button>" should disappear

    	Examples:
    	| item         | buy_button | price_tag | 
    	| item teste 1 | buy1		| 100       |
        | item teste 2 | buy2       | 234       |


    @another_sample_tag
    Scenario: Sample Scenario 3
        Given the "Adventure 1" page is loaded
        When I click on "Go Home Button"
        Then the "Home Screen" should be loaded